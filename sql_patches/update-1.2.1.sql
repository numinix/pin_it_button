SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration_group
WHERE configuration_group_title= 'Pinterest.com Pin-It Button'
LIMIT 1;

REPLACE INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES
(NULL, 'Version', 'PIN_IT_BUTTON_VERSION', '1.2.1', 'Version Installed:', @configuration_group_id, 0, NOW(), NULL, NULL);

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES
(NULL, 'Pinterest Method', 'PINTEREST_BUTTON_METHOD', 'basic', 'Use the basic method (for single pin-it buttons per page) or the advanced method (for multiple buttons - asynchronous):', @configuration_group_id, 20, NOW(), NULL, 'zen_cfg_select_option(array(\'basic\', \'advanced\'),');