SET @configuration_group_id=0;
SELECT @configuration_group_id:=configuration_group_id
FROM configuration_group
WHERE configuration_group_title= 'Pinterest.com Pin-It Button'
LIMIT 1;
DELETE FROM configuration WHERE configuration_group_id = @configuration_group_id;
DELETE FROM configuration_group WHERE configuration_group_id = @configuration_group_id;

INSERT INTO configuration_group (configuration_group_id, configuration_group_title, configuration_group_description, sort_order, visible) VALUES (NULL, 'Pinterest.com Pin-It Button', 'Set Pinterest.com Pin-It Button Options', '1', '1');
SET @configuration_group_id=last_insert_id();
UPDATE configuration_group SET sort_order = @configuration_group_id WHERE configuration_group_id = @configuration_group_id;

INSERT INTO configuration (configuration_id, configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added, use_function, set_function) VALUES
(NULL, 'Enable Pinterest Button', 'PINTEREST_BUTTON_STATUS', 'false', 'Enable the Pinterest.com Pin It Button?', @configuration_group_id, 0, NOW(), NULL, 'zen_cfg_select_option(array(\'true\', \'false\'),'), 
(NULL, 'Pinterest Button Count', 'PINTEREST_BUTTON_COUNT', 'none', 'Display the count horizontally, vertically, or disable (none)', @configuration_group_id, 10, NOW(), NULL, 'zen_cfg_select_option(array(\'none\', \'vertical\', \'horizontal\'),');